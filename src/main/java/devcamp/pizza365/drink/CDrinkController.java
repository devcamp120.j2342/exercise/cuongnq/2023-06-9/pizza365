package devcamp.pizza365.drink;



import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CDrinkController {
    	@CrossOrigin
	@GetMapping("/drink")
	public ArrayList<CDrink> getdrink(){
		
		ArrayList<CDrink> drinks = new ArrayList<CDrink>();
		CDrink drink1 = new CDrink("TRA TAC", "trà tắc", 10000, null, "11/6/2023", null);
		CDrink drink2 = new CDrink("COCA", "Cocacola", 15000 , null ,"11/6/2023", null);
		CDrink drink3 = new CDrink("LAVIE", "lavie", 5000, null, "11/6/2023", null);
        CDrink drink4 = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, "11/6/2023", null);
        CDrink drink5 = new CDrink("FANTA", "Fanta", 15000, null, "11/6/2023", null);
		//TODO Logic here.
		
		drinks.add(drink1);
		drinks.add(drink2);
		drinks.add(drink3);
        drinks.add(drink4);
        drinks.add(drink5);
		
		return drinks;
	}
}
