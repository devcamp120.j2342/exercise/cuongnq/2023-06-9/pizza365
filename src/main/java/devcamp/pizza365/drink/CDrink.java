package devcamp.pizza365.drink;



public class CDrink {
    private String maNuocUong;
    private String tenNuocUong;
    private int donGia;
    private String ghiChu;
    private String ngayTao;
    private String ngayCapNhat;
    public CDrink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, String ngayTao,
            String ngayCapNhat) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public int getDonGia() {
        return donGia;
    }
    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public String getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }
    public String getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(String ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    @Override
    public String toString() {
        return "CDrinkController [maNuocUong=" + maNuocUong + ", tenNuocUong=" + tenNuocUong + ", donGia=" + donGia
                + ", ghiChu=" + ghiChu + ", ngayTao=" + ngayTao + ", ngayCapNhat=" + ngayCapNhat + "]";
    }

   
}
