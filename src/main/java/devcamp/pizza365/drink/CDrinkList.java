package devcamp.pizza365.drink;


import java.util.ArrayList;


public class CDrinkList {
    	ArrayList<CDrink> drinks = new ArrayList<CDrink>();

	public ArrayList<CDrink> getdrinks() {
		return drinks;
	}

	public void setdrinks(ArrayList<CDrink> drinks) {
		this.drinks = drinks;
	}

    @Override
    public String toString() {
        return "CDrinkList [drinks=" + drinks + "]";
    }

  
}
